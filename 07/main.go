package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"sync"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	perms := permutations([]int{0, 1, 2, 3, 4})
	maxSignal := 0
	for _, setting := range perms {
		s := amplify(program, setting)
		if maxSignal < s {
			maxSignal = s
		}
	}

	fmt.Println(maxSignal)

	// Part 2
	perms = permutations([]int{5, 6, 7, 8, 9})
	maxSignal = 0
	for _, setting := range perms {
		s := amplify(program, setting)
		if maxSignal < s {
			maxSignal = s
		}
	}

	fmt.Println(maxSignal)
}

func amplify(program []int, settings []int) int {
	var wg sync.WaitGroup
	wg.Add(5)

	o := []chan int{
		make(chan int, 1),
		make(chan int),
		make(chan int),
		make(chan int),
		make(chan int),
	}

	for i := 0; i < 5; i++ {
		p := make([]int, len(program))
		// Do not forget to copy a program :')
		copy(p, program)
		go executeProgram(p, o[i], o[(i+1)%5], &wg)
		o[i] <- settings[i]
	}

	o[0] <- 0

	wg.Wait()

	return <-o[0]
}

func permutations(array []int) [][]int {
	// First permutation is always first number
	permutations := [][]int{
		[]int{array[0]},
	}
	// For each number in array
	for i := 1; i < len(array); i++ {
		next := [][]int{}
		// Range through currently available permutations
		for _, v := range permutations {
			// And insert next number on every position
			for j := 0; j <= len(v); j++ {
				// Make copy of current permutation
				t := make([]int, len(v))
				copy(t, v)

				// Insert next number on position j
				t = append(t, 0)
				copy(t[j+1:], t[j:])
				t[j] = array[i]

				// Add new permutation to next generation
				next = append(next, t)
			}
		}
		// Set next generation as new permutations
		permutations = next
	}

	return permutations
}

func executeProgram(p []int, input <-chan int, output chan<- int, wg *sync.WaitGroup) {
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode)
		}
		if intcode != 3 && intcode != 4 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode)
		}

		switch intcode {
		// Add
		case 1:
			p[p[i+3]] = par1 + par2
			i += 4
		// Multiply
		case 2:
			p[p[i+3]] = par1 * par2
			i += 4
		// Input
		case 3:
			p[p[i+1]] = <-input
			i += 2
		// Output
		case 4:
			output <- par1
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				p[p[i+3]] = 1
			} else {
				p[p[i+3]] = 0
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				p[p[i+3]] = 1
			} else {
				p[p[i+3]] = 0
			}
			i += 4
		case 99:
			wg.Done()
			return
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program []int, pointer int, mode int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
