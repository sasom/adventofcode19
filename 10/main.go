package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"sort"
	"strings"
)

func main() {
	m, err := readFile("input.txt", "\n")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	point, max := findMaxCount(m)
	fmt.Println(point, max)

	// Part 2
	_, orderedMap := countDetected(m, point[0], point[1])
	fmt.Println(destroyWithLaser(orderedMap, 200))
}

func findMaxCount(m []string) ([]int, int) {
	maxCount := 0
	point := []int{0, 0}
	for y1, line := range m {
		for x1, v := range line {
			if v == '#' {
				count, _ := countDetected(m, x1, y1)
				if count > maxCount {
					maxCount = count
					point = []int{x1, y1}
				}
			}
		}
	}
	return point, maxCount
}

func countDetected(m []string, x1, y1 int) (int, map[float64][][]int) {
	// For second part map needs to be ordered
	// Asteroid nearest to selected asteroid should be first in array, for every angle
	orderedMap := make(map[float64][][]int)

	// Map upper half in correct order
	for y2 := y1; y2 >= 0; y2-- {
		for x2, v := range m[y2] {
			if y2 == y1 && x2 == x1 {
				break
			}
			if v == '#' {
				p := orderedMap[calcAngle(x1, y1, x2, y2)]
				p = append(p, []int{x2, y2})
				orderedMap[calcAngle(x1, y1, x2, y2)] = p
			}
		}
	}

	// Map lower half in correct order
	for y2 := y1; y2 < len(m); y2++ {
		for x2, v := range m[y2] {
			if y2 == y1 && x2 <= x1 {
				continue
			}
			if v == '#' {
				p := orderedMap[calcAngle(x1, y1, x2, y2)]
				p = append(p, []int{x2, y2})
				orderedMap[calcAngle(x1, y1, x2, y2)] = p
			}
		}
	}

	return len(orderedMap), orderedMap
}

func destroyWithLaser(orderedMap map[float64][][]int, bet int) []int {
	// Sort keys
	keys := make([]float64, 0, len(orderedMap))
	for k := range orderedMap {
		keys = append(keys, k)
	}
	sort.Float64s(keys)

	// Destroy
	laser := 0
	destroyed := 0
	for {
		i := keys[laser%len(keys)]
		p := orderedMap[i]

		// Hit
		if len(p) > 0 {
			// Bet reached
			if destroyed == bet-1 {
				return p[0]
			}

			// Remove first asteroid for that angle
			p = p[1:]
			orderedMap[i] = p
			destroyed++
		}

		laser++
	}
}

func calcAngle(x1, y1, x2, y2 int) float64 {
	a := math.Atan2(0, 1) - math.Atan2(float64(x1-x2), float64(y1-y2))
	if a < 0 {
		a += 2 * math.Pi
	}
	return a
}

func readFile(path string, delimiter string) ([]string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	return strings.Split(s, delimiter), nil
}
