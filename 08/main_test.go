package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateImage(t *testing.T) {
	tests := []struct {
		width  int
		height int
		data   string
		want   int
	}{
		{
			width:  3,
			height: 2,
			data:   "121212012012",
			want:   9,
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := validateImage(tt.width, tt.height, tt.data)

			assert.Equal(t, tt.want, got)
		})
	}
}

func TestDecodeImage(t *testing.T) {
	tests := []struct {
		width  int
		height int
		data   string
		want   []rune
	}{
		{
			width:  2,
			height: 2,
			data:   "0222112222120000",
			want:   []rune{'0', '1', '1', '0'},
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := decodeImage(tt.width, tt.height, tt.data)

			assert.Equal(t, tt.want, got)
		})
	}
}
