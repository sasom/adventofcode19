package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strings"
)

func main() {
	imageWidth := 25
	imageHeight := 6
	imageData, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	fmt.Println(validateImage(imageWidth, imageHeight, imageData))

	// Part 2
	image := decodeImage(imageWidth, imageHeight, imageData)
	drawImage(imageWidth, imageHeight, image)
}

func validateImage(width, height int, data string) int {
	x, y := 0, 0
	count := make(map[rune]int)
	result := []int{math.MaxInt32, 0}
	for _, v := range data {
		if x == width {
			x = 0
			y++
		}
		if y == height {
			y = 0
			// Store layer with fewest 0 digits
			if result[0] > count['0'] {
				result[0] = count['0']
				result[1] = count['1'] * count['2']
			}
			// Reset counter
			count['0'], count['1'], count['2'] = 0, 0, 0
		}
		x++
		count[v]++
	}

	return result[1]
}

func decodeImage(width, height int, data string) []rune {
	image := make([]rune, width*height)
	x, y := 0, 0
	for i, v := range data {
		if x == width {
			x = 0
			y++
		}
		if y == height {
			y = 0
		}

		// Draw first layer
		// For other layers draw only on transparent
		if i < width*height {
			image[y*width+x] = v
		} else if image[y*width+x] == '2' {
			image[y*width+x] = v
		}

		x++
	}

	return image
}

func drawImage(width, height int, image []rune) {

	// Draw decoded image
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			switch image[y*width+x] {
			case '0':
				fmt.Print(" ")
			case '1':
				fmt.Print("O")
			default:
				fmt.Print("X")
			}
		}
		fmt.Println()
	}
}

func readFile(path string) (string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	return s, nil
}
