package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"reflect"
	"strings"
	"time"
)

const maxKeys = 'z' - 'a' + 1

type point struct {
	x int
	y int
}

type state struct {
	position point
	keys     [maxKeys]bool
}

type path struct {
	state
	distance int
}

func (p *path) move(po point) {
	p.state.position.x += po.x
	p.state.position.y += po.y
	p.distance++
}

func main() {
	//Part 1
	maze, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	start, keysToFind := findStartAndKeys(maze)
	t := time.Now()
	distance := shortestPath(maze, start, keysToFind, false)
	fmt.Println(distance, time.Since(t))

	// Part 2
	maze, err = readFile("input2.txt")
	if err != nil {
		log.Fatal(err)
	}

	mazes := divideMaze(maze)
	min := 0
	t = time.Now()
	for _, m := range mazes {
		start, keysToFind := findStartAndKeys(m)
		min += shortestPath(m, start, keysToFind, true)
	}
	fmt.Println(min, time.Since(t))
}

func divideMaze(maze []string) [4][]string {
	m := [4][]string{}
	for _, l := range maze[:41] {
		m[0] = append(m[0], l[:41])
	}

	for _, l := range maze[:41] {
		m[1] = append(m[1], l[40:])
	}

	for _, l := range maze[40:] {
		m[2] = append(m[2], l[:41])
	}

	for _, l := range maze[40:] {
		m[3] = append(m[3], l[40:])
	}

	return m
}

func findStartAndKeys(maze []string) (point, [maxKeys]bool) {
	var start point
	keys := [maxKeys]bool{}
	for y, l := range maze {
		for x, v := range l {
			if v == '@' {
				start = point{x, y}
			} else if v >= 'a' && v <= 'z' {
				keys[v-'a'] = true
			}
		}
	}

	return start, keys
}

func shortestPath(maze []string, start point, keysToFind [maxKeys]bool, openDoors bool) int {
	directions := []point{
		point{0, 1},
		point{-1, 0},
		point{0, -1},
		point{1, 0},
	}

	startPath := path{
		state: state{
			keys:     [maxKeys]bool{},
			position: start,
		},
		distance: 0,
	}

	queue := []path{
		startPath,
	}
	memo := map[state]bool{
		startPath.state: true,
	}
	for len(queue) > 0 {
		path := queue[0]
		queue = queue[1:]
		for _, d := range directions {
			newPath := path
			newPath.move(d)

			if memo[newPath.state] {
				continue
			}
			memo[newPath.state] = true

			addNewKey(maze, &newPath)
			if reflect.DeepEqual(keysToFind, newPath.state.keys) {
				return newPath.distance
			}

			if canMove(maze, newPath, openDoors) {
				queue = append(queue, newPath)
			}
		}
	}

	return -1
}

func addNewKey(maze []string, p *path) {
	x := p.state.position.x
	y := p.state.position.y
	// Not Key
	if !(rune(maze[y][x]) >= 'a' &&
		rune(maze[y][x]) <= 'z') {
		return
	}

	r := rune(maze[y][x])
	key := r - 'a'
	if p.state.keys[key] {
		return
	}
	p.state.keys[key] = true
}

func canMove(maze []string, p path, openDoors bool) bool {
	x := p.state.position.x
	y := p.state.position.y
	if maze[y][x] == '.' ||
		maze[y][x] == '@' {
		return true
	}
	// Key
	if rune(maze[y][x]) >= 'a' &&
		rune(maze[y][x]) <= 'z' {
		return true
	}
	// Door
	if rune(maze[y][x]) >= 'A' &&
		rune(maze[y][x]) <= 'Z' {

		// For part 2
		if openDoors {
			return true
		}

		door := strings.ToLower(string(maze[y][x]))
		key := door[0] - 'a'
		haveKey := p.state.keys[key]
		if haveKey {
			return true
		}
	}
	return false
}

func readFile(path string) ([]string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	return strings.Split(s, "\n"), nil
}
