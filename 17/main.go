package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	up    int = 0
	down      = 1
	left      = 2
	right     = 3
)

type point struct {
	x int
	y int
}

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	programMemory1 := make(map[int]int)
	programMemory2 := make(map[int]int)
	for i, v := range program {
		programMemory1[i] = v
		programMemory2[i] = v
	}

	// Part 1
	picture := getPicture(programMemory1)
	drawMap(picture)
	fmt.Println(countIntersections(picture))
	fmt.Println()

	// Part 2
	path := findPath(picture)
	fmt.Println(path)
	fmt.Println()

	main, A, B, C := findFunctions(path)
	fmt.Println("Main", main)
	fmt.Println("A", A)
	fmt.Println("B", B)
	fmt.Println("C", C)
	fmt.Println()

	fmt.Println(getDust(programMemory2, main, A, B, C))
}

func getDust(programMemory map[int]int, main, A, B, C string) (dust int) {
	programMemory[0] = 2

	inputLength := len(main) + len(A) + len(B) + len(C) + 4 + 2
	ci := make(chan int, inputLength)
	co := make(chan int)

	feedInput(main, ci)
	feedInput(A, ci)
	feedInput(B, ci)
	feedInput(C, ci)
	feedInput("n", ci)

	go executeProgram(programMemory, ci, co)
	for v := range co {
		dust = v
	}

	return dust
}

func feedInput(s string, input chan<- int) {
	for _, v := range s {
		input <- int(v)
	}
	input <- '\n'
}

func findFunctions(path string) (string, string, string, string) {
	a, b, c := 20, 20, 20
	A, B, C := "", "", ""
	erased := ""
	copy := ""
	for a > 0 {
		copy = path
		erased = ""
		tmp := ""
		// Find pattern in first 20 chars, starting with the longest
		// Replace all occurrences with A
		copy, A, a = findNextPattern(copy, "A", a)
		// Remove all A or ',' at the start
		copy, tmp = eraseFunctionsAtStart(copy)
		erased += tmp

		// Find pattern for B and replace all occurrences with B
		copy, B, b = findNextPattern(copy, "B", b)
		// Remove all A, B and ',' at the start
		copy, tmp = eraseFunctionsAtStart(copy)
		erased += tmp

		// Find pattern for C and replace all occurrences with C
		copy, C, c = findNextPattern(copy, "C", c)

		// If string still contains numbers or R and L
		// Repeat
		if strings.ContainsAny(copy, "1234567890RL") {
			if c != 0 {
				c--
			} else if b != 0 {
				c = 20
				b--
			} else if a != 0 {
				b, c = 20, 20
				a--
			}
		} else {
			break
		}
	}

	main := erased + copy
	return main, A, B, C
}

func eraseFunctionsAtStart(path string) (string, string) {
	erased := ""
	for _, v := range path {
		if v == 'A' || v == 'B' || v == ',' {
			erased += string(v)
			path = path[1:]
		} else {
			break
		}
	}

	return path, erased
}

func findNextPattern(path string, letter string, i int) (string, string, int) {
	pattern := ""
	for i > 0 {
		if !strings.Contains(path[i:], path[:i]) ||
			strings.ContainsAny(path[:i], "ABC") ||
			path[i-1] == ',' {
			i--
		}
		pattern = path[:i]
		path = strings.ReplaceAll(path, pattern, letter)
		break
	}

	return path, pattern, i
}

// TODO This could be written nicely
func findPath(picture []string) string {
	var p point
	var o int
	for y, line := range picture {
		for x, v := range line {
			if v == '^' {
				p = point{x, y}
				o = up
			} else if v == '>' {
				p = point{x, y}
				o = right
			} else if v == '<' {
				p = point{x, y}
				o = left
			} else if v == 'v' {
				p = point{x, y}
				o = down
			}
		}
	}

	count := 0
	cs := ""
	s := ""
	for {
		cs = strconv.Itoa(count)
		if o == up {
			if p.y != 0 && picture[p.y-1][p.x] == '#' {
				p.y--
				count++
			} else if p.x != 0 && picture[p.y][p.x-1] == '#' {
				o = left
				s += cs + ",L,"
				count = 0
			} else if p.x != len(picture[p.y])-1 && picture[p.y][p.x+1] == '#' {
				o = right
				s += cs + ",R,"
				count = 0
			} else {
				break
			}
		} else if o == down {
			if p.y != len(picture)-1 && picture[p.y+1][p.x] == '#' {
				p.y++
				count++
			} else if p.x != 0 && picture[p.y][p.x-1] == '#' {
				o = left
				s += cs + ",R,"
				count = 0
			} else if p.x != len(picture[p.y])-1 && picture[p.y][p.x+1] == '#' {
				o = right
				s += cs + ",L,"
				count = 0
			} else {
				break
			}
		} else if o == left {
			if p.x != 0 && picture[p.y][p.x-1] == '#' {
				p.x--
				count++
			} else if p.y != 0 && picture[p.y-1][p.x] == '#' {
				o = up
				s += cs + ",R,"
				count = 0
			} else if p.y != len(picture[p.y])-1 && picture[p.y+1][p.x] == '#' {
				o = down
				s += cs + ",L,"
				count = 0
			} else {
				break
			}
		} else if o == right {
			if p.x != len(picture[p.y])-1 && picture[p.y][p.x+1] == '#' {
				p.x++
				count++
			} else if p.y != 0 && picture[p.y-1][p.x] == '#' {
				o = up
				s += cs + ",L,"
				count = 0
			} else if p.y != len(picture[p.y])-1 && picture[p.y+1][p.x] == '#' {
				o = down
				s += cs + ",R,"
				count = 0
			} else {
				break
			}
		}
	}
	s += cs
	return s[2:]
}

func drawMap(picture []string) {
	for _, line := range picture {
		for _, v := range line {
			fmt.Printf(string(v))
		}
		fmt.Println()
	}
}

func countIntersections(picture []string) int {
	sum := 0
	for y := 1; y < len(picture)-1; y++ {
		for x := 1; x < len(picture[y])-1; x++ {
			if picture[y][x] == '#' &&
				picture[y+1][x] == '#' &&
				picture[y-1][x] == '#' &&
				picture[y][x+1] == '#' &&
				picture[y][x-1] == '#' {
				sum += x * y
			}
		}
	}

	return sum
}

func getPicture(programMemory map[int]int) []string {
	ci := make(chan int)
	co := make(chan int)
	go executeProgram(programMemory, ci, co)

	s := ""
	for v := range co {
		s += string(v)
	}

	for s[len(s)-1] == '\n' {
		s = strings.Trim(s, "\n")
	}

	return strings.Split(s, "\n")
}

func executeProgram(p map[int]int, input <-chan int, output chan<- int) {
	relativeBase := 0
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode, relativeBase)
		}
		if intcode != 3 && intcode != 4 && intcode != 9 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode, relativeBase)
		}

		switch intcode {
		// Add
		case 1:
			write(p, par1+par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Multiply
		case 2:
			write(p, par1*par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Input
		case 3:
			write(p, <-input, i+1, (p[i]/100)%10, relativeBase)
			i += 2
		// Output
		case 4:
			output <- par1
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Adjust relative base
		case 9:
			relativeBase += par1
			i += 2
		case 99:
			close(output)
			return
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program map[int]int, pointer int, mode int, relativeBase int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	// Relative mode
	case 2:
		return program[relativeBase+program[pointer]]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func write(program map[int]int, input int, pointer int, mode int, relativeBase int) {
	switch mode {
	// Position mode
	case 0:
		program[program[pointer]] = input
	// Immediate mode
	case 1:
		program[pointer] = input
	// Relative mode
	case 2:
		program[relativeBase+program[pointer]] = input
	default:
		log.Fatal("Invalid parameter mode")
	}
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
