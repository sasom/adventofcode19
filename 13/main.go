package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	programMemory1 := make(map[int]int)
	programMemory2 := make(map[int]int)
	for i, v := range program {
		programMemory1[i] = v
		programMemory2[i] = v
	}

	// Part 1
	fmt.Println(countBlocks(programMemory1))

	// Part 2
	programMemory2[0] = 2
	fmt.Println(play(programMemory2))
}

func countBlocks(programMemory map[int]int) (count int) {
	ci := make(chan int)
	co := make(chan int)
	go executeProgram(programMemory, ci, co)

	for v := range co {
		v = <-co
		v = <-co
		// Block
		if v == 2 {
			count++
		}
	}

	return count
}

func play(programMemory map[int]int) int {
	ci := make(chan int)
	co := make(chan int)
	go executeProgram(programMemory, ci, co)

	var halt bool
	var ball, paddle point
	input, score := 0, 0
	for {
		select {
		case ci <- input:
		case v, ok := <-co:
			p := point{0, 0}
			p.x = v
			p.y, ok = <-co
			titleID, ok := <-co
			halt = ok

			if titleID == 4 {
				ball = p
			} else if titleID == 3 {
				paddle = p
			} else if titleID > 4 {
				score = titleID
			}

			if ball.x < paddle.x {
				input = -1
			} else if ball.x > paddle.x {
				input = 1
			} else {
				input = 0
			}
		}

		if !halt {
			break
		}
	}

	return score
}

func executeProgram(p map[int]int, input <-chan int, output chan<- int) {
	relativeBase := 0
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode, relativeBase)
		}
		if intcode != 3 && intcode != 4 && intcode != 9 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode, relativeBase)
		}

		switch intcode {
		// Add
		case 1:
			write(p, par1+par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Multiply
		case 2:
			write(p, par1*par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Input
		case 3:
			write(p, <-input, i+1, (p[i]/100)%10, relativeBase)
			i += 2
		// Output
		case 4:
			output <- par1
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Adjust relative base
		case 9:
			relativeBase += par1
			i += 2
		case 99:
			close(output)
			return
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program map[int]int, pointer int, mode int, relativeBase int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	// Relative mode
	case 2:
		return program[relativeBase+program[pointer]]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func write(program map[int]int, input int, pointer int, mode int, relativeBase int) {
	switch mode {
	// Position mode
	case 0:
		program[program[pointer]] = input
	// Immediate mode
	case 1:
		program[pointer] = input
	// Relative mode
	case 2:
		program[relativeBase+program[pointer]] = input
	default:
		log.Fatal("Invalid parameter mode")
	}
}

type point struct {
	x int
	y int
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
