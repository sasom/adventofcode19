package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

const (
	up    int = 0
	down      = 1
	left      = 2
	right     = 3
)

type point struct {
	x int
	y int
}

func (p *point) moveBy(p2 point) {
	p.x += p2.x
	p.y += p2.y
}

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	programMemory := make(map[int]int)
	for i, v := range program {
		programMemory[i] = v
	}

	// Part 1
	maze, oxygen := exploreAndMeasureMaze(programMemory, false)
	fmt.Println(maze[oxygen])

	// Part 2
	maze, _ = exploreAndMeasureMaze(programMemory, true)
	max := 0
	for _, v := range maze {
		if max < v {
			max = v
		}
	}
	fmt.Println(max)
}

func exploreAndMeasureMaze(p map[int]int, startAtOxygen bool) (map[point]int, point) {
	ci := make(chan int)
	co := make(chan int)
	go executeProgram(p, ci, co)

	startMeasuring := !startAtOxygen
	maze := make(map[point]int)
	position := point{0, 0}
	var oxygen point
	move := []point{
		point{0, 1},
		point{0, -1},
		point{-1, 0},
		point{1, 0},
	}
	moveIndex := 0
	halt := false
	for !halt {
		select {
		case ci <- moveIndex + 1:
		case v := <-co:
			// Wall
			if v == 0 {
				switch moveIndex {
				case up:
					moveIndex = left
				case left:
					moveIndex = down
				case down:
					moveIndex = right
				case right:
					moveIndex = up
				}
			}
			// Not wall
			if v != 0 {
				// Calc shortest length of current position
				prevPosition := maze[position]
				position.moveBy(move[moveIndex])
				currentPosition := maze[position]
				if currentPosition == 0 && startMeasuring == true {
					maze[position] = prevPosition + 1
				}

				switch moveIndex {
				case up:
					moveIndex = right
				case right:
					moveIndex = down
				case down:
					moveIndex = left
				case left:
					moveIndex = up
				}

				// Different end conditions depending on starting point
				if startAtOxygen {
					if position.x == oxygen.x && position.y == oxygen.y && startMeasuring == true {
						halt = true
					}
				} else {
					if position.x == 0 && position.y == 0 {
						halt = true
					}
				}

				if v == 2 {
					oxygen = position
					startMeasuring = true
				}
			}
		}
	}

	return maze, oxygen
}

func executeProgram(p map[int]int, input <-chan int, output chan<- int) {
	relativeBase := 0
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode, relativeBase)
		}
		if intcode != 3 && intcode != 4 && intcode != 9 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode, relativeBase)
		}

		switch intcode {
		// Add
		case 1:
			write(p, par1+par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Multiply
		case 2:
			write(p, par1*par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Input
		case 3:
			write(p, <-input, i+1, (p[i]/100)%10, relativeBase)
			i += 2
		// Output
		case 4:
			output <- par1
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Adjust relative base
		case 9:
			relativeBase += par1
			i += 2
		case 99:
			close(output)
			return
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program map[int]int, pointer int, mode int, relativeBase int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	// Relative mode
	case 2:
		return program[relativeBase+program[pointer]]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func write(program map[int]int, input int, pointer int, mode int, relativeBase int) {
	switch mode {
	// Position mode
	case 0:
		program[program[pointer]] = input
	// Immediate mode
	case 1:
		program[pointer] = input
	// Relative mode
	case 2:
		program[relativeBase+program[pointer]] = input
	default:
		log.Fatal("Invalid parameter mode")
	}
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
