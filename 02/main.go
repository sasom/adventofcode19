package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	p, err := executeProgram(program, 12, 2)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(p[0])

	// Part 2
	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			p, _ = executeProgram(program, noun, verb)
			if p != nil && p[0] == 19690720 {
				fmt.Println(100*noun + verb)
				return
			}
		}
	}
}

func executeProgram(program []int, noun, verb int) ([]int, error) {
	// Reset the computer's memory to the values in the program
	p := make([]int, len(program))
	copy(p, program)
	p[1] = noun
	p[2] = verb

	for i := 0; i < len(p); i += 4 {
		switch p[i] {
		case 1:
			p[p[i+3]] = p[p[i+1]] + p[p[i+2]]
		case 2:
			p[p[i+3]] = p[p[i+1]] * p[p[i+2]]
		case 99:
			return p, nil
		default:
			return nil, errors.New("Invalid opcode")
		}
	}
	return nil, errors.New("Program executed incorrectly")
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
