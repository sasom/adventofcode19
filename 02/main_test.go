package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExecuteProgram(t *testing.T) {
	tests := []struct {
		input []int
		want  []int
	}{
		{
			input: []int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50},
			want:  []int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50},
		},
		{
			input: []int{1, 0, 0, 0, 99},
			want:  []int{2, 0, 0, 0, 99},
		},
		{
			input: []int{2, 3, 0, 3, 99},
			want:  []int{2, 3, 0, 6, 99},
		},
		{
			input: []int{2, 4, 4, 5, 99, 0},
			want:  []int{2, 4, 4, 5, 99, 9801},
		},
		{
			input: []int{2, 4, 4, 5, 99, 0},
			want:  []int{2, 4, 4, 5, 99, 9801},
		},
		{
			input: []int{1, 1, 1, 4, 99, 5, 6, 0, 99},
			want:  []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("Program %v should execute to %v ", tt.input, tt.want)
		t.Run(name, func(t *testing.T) {
			got, _ := executeProgram(tt.input, tt.input[1], tt.input[2])

			assert.Equal(t, tt.want, got)
		})
	}
}
