package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidPassword(t *testing.T) {
	tests := []struct {
		input string
		want  bool
	}{
		{
			input: "122345",
			want:  true,
		},
		{
			input: "112233",
			want:  true,
		},
		{
			input: "111111",
			want:  true,
		},
		{
			input: "223450",
			want:  false,
		},
		{
			input: "123789",
			want:  false,
		},
		{
			input: "135679",
			want:  false,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("Password %s should be %t", tt.input, tt.want)
		t.Run(name, func(t *testing.T) {
			got := validPassword(tt.input)

			assert.Equal(t, tt.want, got)
		})
	}
}

func TestExtraValidation(t *testing.T) {
	tests := []struct {
		input string
		want  bool
	}{
		{
			input: "123444",
			want:  false,
		},
		{
			input: "112233",
			want:  true,
		},
		{
			input: "111111",
			want:  false,
		},
		{
			input: "223450",
			want:  false,
		},
		{
			input: "123789",
			want:  false,
		},
		{
			input: "135679",
			want:  false,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("Password %s should be %t", tt.input, tt.want)
		t.Run(name, func(t *testing.T) {
			got := validPassword(tt.input)
			if got {
				got = extraValidation(tt.input)
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
