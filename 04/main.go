package main

import (
	"fmt"
	"strconv"
)

func main() {
	minPass := 234208
	maxPass := 765869

	count := 0
	count2 := 0
	for i := minPass; i <= maxPass; i++ {
		if validPassword(strconv.Itoa(i)) {
			count++
			if extraValidation(strconv.Itoa(i)) {
				count2++
			}
		}
	}

	fmt.Println(count)
	fmt.Println(count2)
}

func validPassword(pass string) (valid bool) {
	if len(pass) != 6 {
		return false
	}

	if pass[0] > pass[1] ||
		pass[1] > pass[2] ||
		pass[2] > pass[3] ||
		pass[3] > pass[4] ||
		pass[4] > pass[5] {
		return false
	}

	if pass[0] != pass[1] &&
		pass[1] != pass[2] &&
		pass[2] != pass[3] &&
		pass[3] != pass[4] &&
		pass[4] != pass[5] {
		return false
	}

	return true
}

func extraValidation(pass string) (valid bool) {
	if len(pass) != 6 {
		return false
	}

	if pass[0] == pass[1] &&
		pass[1] != pass[2] {
		return true
	}

	if pass[4] == pass[5] &&
		pass[3] != pass[4] {
		return true
	}

	for i := 1; i <= 3; i++ {
		if pass[i] == pass[i+1] &&
			pass[i-1] != pass[i] &&
			pass[i+1] != pass[i+2] {
			return true
		}
	}

	return false
}
