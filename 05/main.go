package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	executeProgram(program, 1)
	executeProgram(program, 5)
}

func executeProgram(program []int, input int) (output int) {
	// Reset the computer's memory to the values in the program
	p := make([]int, len(program))
	copy(p, program)

	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode)
		}
		if intcode != 3 && intcode != 4 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode)
		}

		switch intcode {
		// Add
		case 1:
			p[p[i+3]] = par1 + par2
			i += 4
		// Multiply
		case 2:
			p[p[i+3]] = par1 * par2
			i += 4
		// Input
		case 3:
			p[p[i+1]] = input
			i += 2
		// Output
		case 4:
			output = par1
			fmt.Println(par1)
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				p[p[i+3]] = 1
			} else {
				p[p[i+3]] = 0
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				p[p[i+3]] = 1
			} else {
				p[p[i+3]] = 0
			}
			i += 4
		case 99:
			return output
		default:
			return -1
		}
	}
}

func paramMode(program []int, pointer int, mode int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
