package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	orbits, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(countOrbits(orbits))
	fmt.Println(findPath(orbits))
}

func countOrbits(orbits map[string]string) int {
	count := 0
	for k, v := range orbits {
		count++
		for v != "COM" {
			count++
			k = v
			v = orbits[k]
		}
	}

	return count
}

func findPath(orbits map[string]string) int {
	m := make(map[string]int)

	// Find my path to COM and save distances to each object on the way
	count := 0
	k := "YOU"
	v := orbits[k]
	for v != "COM" {
		count++
		m[k] = count
		k = v
		v = orbits[k]
	}

	// Find path to object on my path and add SAN distance
	count = 0
	k = "SAN"
	v = orbits[k]
	for m[k] == 0 {
		k = v
		v = orbits[k]
		count++
	}

	// Remove starting points, -2
	// Remove same point on one path, -1
	return m[k] + count - 3
}

func readFile(path string) (map[string]string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	orbits := make(map[string]string)
	for _, line := range strings.Split(s, "\n") {
		orbit := strings.Split(line, ")")
		orbits[orbit[1]] = orbit[0]
	}

	return orbits, nil
}
