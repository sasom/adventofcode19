package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCountOrbits(t *testing.T) {
	tests := []struct {
		input map[string]string
		want  int
	}{
		{
			input: map[string]string{
				"B": "COM",
				"C": "B",
				"D": "C",
				"E": "D",
				"F": "E",
				"G": "B",
				"H": "G",
				"I": "D",
				"J": "E",
				"K": "J",
				"L": "K",
			},
			want: 42,
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := countOrbits(tt.input)

			assert.Equal(t, tt.want, got)
		})
	}
}

func TestFindPath(t *testing.T) {
	tests := []struct {
		input map[string]string
		want  int
	}{
		{
			input: map[string]string{
				"B":   "COM",
				"C":   "B",
				"D":   "C",
				"E":   "D",
				"F":   "E",
				"G":   "B",
				"H":   "G",
				"I":   "D",
				"J":   "E",
				"K":   "J",
				"L":   "K",
				"YOU": "K",
				"SAN": "I",
			},
			want: 4,
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := findPath(tt.input)

			assert.Equal(t, tt.want, got)
		})
	}
}
