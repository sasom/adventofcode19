package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClosestInterception(t *testing.T) {
	tests := []struct {
		wires [2][]string
		want  int
	}{
		{
			wires: [2][]string{
				[]string{"R8", "U5", "L5", "D3"},
				[]string{"U7", "R6", "D4", "L4"},
			},
			want: 6,
		},
		{
			wires: [2][]string{
				[]string{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"},
				[]string{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"},
			},
			want: 159,
		},
		{
			wires: [2][]string{
				[]string{"R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"},
				[]string{"U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"},
			},
			want: 135,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("The closest intersection of %s and %s should be %d units away", tt.wires[0], tt.wires[1], tt.want)
		t.Run(name, func(t *testing.T) {
			placedWire := placeWire(tt.wires[0])
			intersections := getIntersections(placedWire, tt.wires[1])
			got := closestIntersection(intersections)

			assert.Equal(t, tt.want, got)
		})
	}
}

func TestShortestPath(t *testing.T) {
	tests := []struct {
		wires [2][]string
		want  int
	}{
		{
			wires: [2][]string{
				[]string{"R8", "U5", "L5", "D3"},
				[]string{"U7", "R6", "D4", "L4"},
			},
			want: 30,
		},
		{
			wires: [2][]string{
				[]string{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"},
				[]string{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"},
			},
			want: 610,
		},
		{
			wires: [2][]string{
				[]string{"R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"},
				[]string{"U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"},
			},
			want: 410,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("The closest intersection of %s and %s should be %d units away", tt.wires[0], tt.wires[1], tt.want)
		t.Run(name, func(t *testing.T) {
			placedWire := placeWire(tt.wires[0])
			intersections := getIntersections(placedWire, tt.wires[1])
			got := shortestPath(intersections)

			assert.Equal(t, tt.want, got)
		})
	}
}
