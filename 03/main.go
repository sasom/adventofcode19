package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

func main() {
	wires, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	placedWire := placeWire(wires[0])
	intersections := getIntersections(placedWire, wires[1])
	fmt.Println(closestIntersection(intersections))
	fmt.Println(shortestPath(intersections))

}

func placeWire(wire []string) map[point]int {
	m := make(map[point]int)
	p := point{0, 0}
	len := 0

	for _, wi := range wire {
		d, n := parseWire(wi)
		dp := getDirectionPoint(d)

		for i := 0; i < n; i++ {
			len++
			p.moveBy(dp)

			// Storing length of wire for every point
			// Keeping the shortest length
			if m[p] == 0 {
				m[p] = len
			}
		}
	}
	return m
}

func getIntersections(placedWire map[point]int, wire []string) map[point]int {
	intersections := make(map[point]int)
	p := point{0, 0}
	len := 0

	for _, wi := range wire {
		d, n := parseWire(wi)
		dp := getDirectionPoint(d)

		for i := 0; i < n; i++ {
			p.moveBy(dp)
			len++

			// Storing only intersection points with length of both wires
			// Again keeping the shortest length
			if intersections[p] == 0 && placedWire[p] != 0 {
				intersections[p] = placedWire[p] + len
			}
		}
	}
	return intersections
}

// Returns distance between intersection point closest
// to the central port and central port
func closestIntersection(intersections map[point]int) (minD int) {
	for k := range intersections {
		d := k.manhattanDistance()
		if d < minD || minD == 0 {
			minD = d
		}
	}

	return minD
}

// Returns the shortest path two wires can make
func shortestPath(intersections map[point]int) (minD int) {
	for _, v := range intersections {
		if v < minD || minD == 0 {
			minD = v
		}
	}

	return minD
}

func getDirectionPoint(direction string) (p point) {
	switch direction {
	case "U":
		p = point{0, 1}
	case "R":
		p = point{1, 0}
	case "D":
		p = point{0, -1}
	case "L":
		p = point{-1, 0}
	}

	return p
}

func parseWire(s string) (direction string, n int) {
	direction = string(s[0])
	n, err := strconv.Atoi(s[1:])
	if err != nil {
		log.Fatal(err)
	}

	return direction, n
}

func readFile(path string) ([2][]string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	wires := [2][]string{}
	for i, line := range strings.Split(s, "\n") {
		for _, wire := range strings.Split(line, ",") {
			wires[i] = append(wires[i], wire)
		}
	}

	return wires, nil
}

type point struct {
	x int
	y int
}

func (p *point) moveBy(p2 point) {
	p.x += p2.x
	p.y += p2.y
}

func (p point) manhattanDistance() int {
	return int(math.Abs(float64(p.x)) + math.Abs(float64(p.y)))
}
