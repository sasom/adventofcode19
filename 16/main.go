package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	signal, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	s := make([]int, len(signal))
	copy(s, signal)

	for i := 0; i < 100; i++ {
		s = nextPhase(s)
	}
	fmt.Println(s[:8])

	// Part 2 - The trick is in offset
	// Offset is larger than len(signal)/2.
	// Offset shifts pattern so much,
	// that it contains only zeros at start and ones at the end.
	// If signal is 1234567 and offset is 4,
	// then phases would look like this
	// 0 0 0 0 5 6 7 (0*1 0*2 0*3 0*4 1*5 1*6 1*7)
	// 0 0 0 0 0 6 7
	// 0 0 0 0 0 0 7
	// Therefore we can use only addition,
	// starting at the right side, to compute phases very quickly
	offset, err := arrayToInt(signal[:7])
	if err != nil {
		log.Fatal(err)
	}
	s = make([]int, len(signal)*10000)
	for i := range s {
		s[i] = signal[i%len(signal)]
	}

	for i := 0; i < 100; i++ {
		sum := 0
		for j := len(s) - 1; j >= offset; j-- {
			sum += s[j]
			s[j] = sum % 10
			if s[j] < 0 {
				s[j] = -s[j]
			}
		}
	}
	fmt.Println(s[offset : offset+8])
}

func nextPhase(signal []int) []int {
	next := make([]int, len(signal))
	for i := 0; i < len(signal); i++ {
		sum := 0
		count := 0
		sign := 1
		for j := i; j < len(signal); j++ {
			sum += signal[j] * sign
			count++
			if count == i+1 {
				// Skip zeros
				j += i + 1
				count = 0
				sign *= -1
			}
		}

		next[i] = sum % 10
		if next[i] < 0 {
			next[i] = -next[i]
		}
	}

	return next
}

func arrayToInt(array []int) (int, error) {
	s := fmt.Sprint(array)
	sa := strings.Split(s, " ")
	s = strings.Join(sa, "")
	s = strings.Trim(s, "[]")

	i, err := strconv.Atoi(s)
	if err != nil {
		return -1, err
	}

	return i, nil
}

func readFile(path string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, number := range s {
		i, err := strconv.Atoi(string(number))
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
