package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	programMap := make(map[int]int)
	for i, v := range program {
		programMap[i] = v
	}

	// Part 1
	panels := robotPaint(programMap, 0)
	fmt.Println(len(panels))

	// Part 2
	panels = robotPaint(programMap, 1)
	readPainting(panels)
}

func readPainting(panels map[point]int) {
	// Find top left and bottom right points
	topLeft := point{0, 0}
	bottomRight := point{0, 0}
	for k := range panels {
		if k.x >= bottomRight.x {
			bottomRight.x = k.x
		}
		if k.y >= bottomRight.y {
			bottomRight.y = k.y
		}
		if k.x <= topLeft.x {
			topLeft.x = k.x
		}
		if k.y <= topLeft.y {
			topLeft.y = k.y
		}
	}

	// Draw readable text
	for y := topLeft.y; y <= bottomRight.y; y++ {
		for x := topLeft.x; x <= bottomRight.x; x++ {
			if panels[point{x, y}] == 1 {
				fmt.Print("O")
				continue
			}
			fmt.Print(" ")
		}
		fmt.Println()
	}
}

func robotPaint(programMap map[int]int, startColor int) map[point]int {
	panels := make(map[point]int)
	position := point{0, 0}
	movement := []point{
		point{0, -1},
		point{1, 0},
		point{0, 1},
		point{-1, 0},
	}
	movementIndex := 0

	ci := make(chan int, 1)
	co := make(chan int)
	go executeProgram(programMap, ci, co)

	ci <- startColor
	for color := range co {
		panels[position] = color

		direction := <-co

		// Turn left
		if direction == 0 {
			movementIndex--
			if movementIndex < 0 {
				movementIndex = 3
			}
		} else if direction == 1 {
			movementIndex++
			if movementIndex > 3 {
				movementIndex = 0
			}
		}

		position.moveBy(movement[movementIndex])

		ci <- panels[position]
	}

	return panels
}

func executeProgram(p map[int]int, input <-chan int, output chan<- int) {
	relativeBase := 0
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode, relativeBase)
		}
		if intcode != 3 && intcode != 4 && intcode != 9 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode, relativeBase)
		}

		switch intcode {
		// Add
		case 1:
			write(p, par1+par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Multiply
		case 2:
			write(p, par1*par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Input
		case 3:
			write(p, <-input, i+1, (p[i]/100)%10, relativeBase)
			i += 2
		// Output
		case 4:
			output <- par1
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Adjust relative base
		case 9:
			relativeBase += par1
			i += 2
		case 99:
			close(output)
			return
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program map[int]int, pointer int, mode int, relativeBase int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	// Relative mode
	case 2:
		return program[relativeBase+program[pointer]]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func write(program map[int]int, input int, pointer int, mode int, relativeBase int) {
	switch mode {
	// Position mode
	case 0:
		program[program[pointer]] = input
	// Immediate mode
	case 1:
		program[pointer] = input
	// Relative mode
	case 2:
		program[relativeBase+program[pointer]] = input
	default:
		log.Fatal("Invalid parameter mode")
	}
}

type point struct {
	x int
	y int
}

func (p *point) moveBy(p2 point) {
	p.x += p2.x
	p.y += p2.y
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
