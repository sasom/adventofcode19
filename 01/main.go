package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	mods, err := readFile("input.txt", "\n")
	if err != nil {
		panic(err)
	}

	// Part 1
	fmt.Println("What is the sum of the fuel requirements for modules?")
	fmt.Println(fuelForMods(mods))

	// Part 2
	fmt.Println("What is the sum of the fuel requirements for modules and fuel?")
	fmt.Println(fuelForModsAndFuel(mods))
}

func fuelForModsAndFuel(mods []int) int {
	total := 0
	for _, m := range mods {
		f := fuelForMass(m)
		total += f
		total += fuelForFuel(f)
	}

	return total
}

func fuelForMods(mods []int) int {
	total := 0
	for _, m := range mods {
		total += fuelForMass(m)
	}

	return total
}

func fuelForFuel(fuel int) int {
	f := fuelForMass(fuel)

	if f < 0 {
		return 0
	}
	return f + fuelForFuel(f)
}

func fuelForMass(mass int) int {
	return mass/3 - 2
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
