package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalculateFuel(t *testing.T) {
	tests := []struct {
		input int
		want  int
	}{
		{
			input: 12,
			want:  2,
		},
		{
			input: 14,
			want:  2,
		},
		{
			input: 1969,
			want:  654,
		},
		{
			input: 100756,
			want:  33583,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("Module with mass %d needs %d fuel", tt.input, tt.want)
		t.Run(name, func(t *testing.T) {
			got := fuelForMass(tt.input)

			assert.Equal(t, tt.want, got)
		})
	}
}

func TestFuelForFuel(t *testing.T) {
	tests := []struct {
		input int
		want  int
	}{
		{
			input: 14,
			want:  2,
		},
		{
			input: 14,
			want:  2,
		},
		{
			input: 1969,
			want:  966,
		},
		{
			input: 100756,
			want:  50346,
		},
	}

	for _, tt := range tests {
		name := fmt.Sprintf("Fuel with mass %d needs %d fuel", tt.input, tt.want)
		t.Run(name, func(t *testing.T) {
			got := fuelForFuel(tt.input)

			assert.Equal(t, tt.want, got)
		})
	}
}
