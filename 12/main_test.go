package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalculateEnergy(t *testing.T) {
	tests := []struct {
		moons  []point
		steps  int
		energy float64
	}{
		{
			moons: []point{
				point{-1, 0, 2},
				point{2, -10, -7},
				point{4, -8, 8},
				point{3, 5, -1},
			},
			steps:  10,
			energy: 179,
		},
		{
			moons: []point{
				point{-8, -10, 0},
				point{5, 5, 10},
				point{2, -7, 3},
				point{9, -8, -3},
			},
			steps:  100,
			energy: 1940,
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := executeSteps(tt.steps, tt.moons)

			assert.Equal(t, tt.energy, got)
		})
	}
}

func TestMatchInSteps(t *testing.T) {
	tests := []struct {
		moons []point
		steps int
	}{
		{
			moons: []point{
				point{-1, 0, 2},
				point{2, -10, -7},
				point{4, -8, 8},
				point{3, 5, -1},
			},
			steps: 2772,
		},
		{
			moons: []point{
				point{-8, -10, 0},
				point{5, 5, 10},
				point{2, -7, 3},
				point{9, -8, -3},
			},
			steps: 4686774924,
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			got := matchInSteps(tt.moons)

			assert.Equal(t, tt.steps, got)
		})
	}
}
