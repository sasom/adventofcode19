package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	moons, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(executeSteps(1000, moons))

	fmt.Println(matchInSteps(moons))
}

func matchInSteps(moons []point) int {
	m := make([]point, len(moons))
	copy(m, moons)
	sv := [4]point{}
	velocities := [4]point{}

	// Each coordinate has its own repetition period
	// However velocity repeats twice while moon coordinates repeat once
	// So we can look only at velocity and divide n of cycles by 2
	periodX := 0
	periodY := 0
	periodZ := 0

	step := 0
	for periodX == 0 || periodY == 0 || periodZ == 0 {
		step++

		velocities = calcVelocity(m, velocities)
		m = adjustPositions(m, velocities)

		if periodX == 0 {
			for i := range sv {
				if velocities[i].x != sv[i].x {
					periodX = 0
					break
				}
				periodX = step
			}
		}

		if periodY == 0 {
			for i := range sv {
				if velocities[i].y != sv[i].y {
					periodY = 0
					break
				}
				periodY = step
			}
		}

		if periodZ == 0 {
			for i := range sv {
				if velocities[i].z != sv[i].z {
					periodZ = 0
					break
				}
				periodZ = step
			}
		}
	}

	// Least Common Multiple x 2 is the solution
	return lcm(periodX, periodY, periodZ) * 2
}

func executeSteps(n int, moons []point) float64 {
	cpMoons := make([]point, len(moons))
	copy(cpMoons, moons)

	velocities := [4]point{}
	for i := 0; i < n; i++ {
		velocities = calcVelocity(cpMoons, velocities)
		cpMoons = adjustPositions(cpMoons, velocities)
	}

	return calcKinAndPotEnergy(cpMoons, velocities)
}

func calcVelocity(moons []point, velocities [4]point) [4]point {
	for i, cm := range moons {
		for _, m := range moons {
			if cm.x < m.x {
				velocities[i].x++
			} else if cm.x > m.x {
				velocities[i].x--
			}

			if cm.y < m.y {
				velocities[i].y++
			} else if cm.y > m.y {
				velocities[i].y--
			}

			if cm.z < m.z {
				velocities[i].z++
			} else if cm.z > m.z {
				velocities[i].z--
			}
		}
	}

	return velocities
}

func adjustPositions(moons []point, velocities [4]point) []point {
	for i := range moons {
		moons[i].Add(velocities[i])
	}

	return moons
}

func calcKinAndPotEnergy(moons []point, velocities [4]point) float64 {
	e := 0.0
	for i := range moons {
		// Potential * Kinetic
		e += energy(moons[i]) * energy(velocities[i])
	}

	return e
}

func energy(p point) float64 {
	return math.Abs(float64(p.x)) + math.Abs(float64(p.y)) + math.Abs(float64(p.z))
}

type point struct {
	x int
	y int
	z int
}

func (p *point) Add(p2 point) {
	p.x += p2.x
	p.y += p2.y
	p.z += p2.z
}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func lcm(a, b int, integers ...int) int {
	result := a * b / gcd(a, b)

	for i := 0; i < len(integers); i++ {
		result = lcm(result, integers[i])
	}

	return result
}

func readFile(path string) ([]point, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	s := strings.TrimSpace(string(f))

	rgx := regexp.MustCompile("<x=(.+), y=(.+), z=(.+)>")
	matches := rgx.FindAllStringSubmatch(s, -1)

	points := []point{}
	for _, line := range matches {
		x, err := strconv.Atoi(line[1])
		if err != nil {
			return nil, err
		}
		y, err := strconv.Atoi(line[2])
		if err != nil {
			return nil, err
		}
		z, err := strconv.Atoi(line[3])
		if err != nil {
			return nil, err
		}
		points = append(points, point{x, y, z})
	}

	return points, nil
}
