package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	program, err := readFile("input.txt", ",")
	if err != nil {
		log.Fatal(err)
	}

	// Part 1
	programMap := make(map[int]int)
	for i, v := range program {
		programMap[i] = v
	}
	fmt.Println(executeProgram(programMap, 1))

	// Part 2
	programMap = make(map[int]int)
	for i, v := range program {
		programMap[i] = v
	}
	fmt.Println(executeProgram(programMap, 2))
}

func executeProgram(p map[int]int, input int) []int {
	output := []int{}

	relativeBase := 0
	i := 0
	for {
		intcode := p[i] % 100
		var par1 int
		var par2 int
		if intcode != 99 {
			mode := (p[i] / 100) % 10
			par1 = paramMode(p, i+1, mode, relativeBase)
		}
		if intcode != 3 && intcode != 4 && intcode != 9 && intcode != 99 {
			mode := (p[i] / 1000) % 10
			par2 = paramMode(p, i+2, mode, relativeBase)
		}

		switch intcode {
		// Add
		case 1:
			write(p, par1+par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Multiply
		case 2:
			write(p, par1*par2, i+3, (p[i]/10000)%10, relativeBase)
			i += 4
		// Input
		case 3:
			write(p, input, i+1, (p[i]/100)%10, relativeBase)
			i += 2
		// Output
		case 4:
			output = append(output, par1)
			i += 2
		// Jump-if-true
		case 5:
			if par1 != 0 {
				i = par2
			} else {
				i += 3
			}
		// Jump-if-false
		case 6:
			if par1 == 0 {
				i = par2
			} else {
				i += 3
			}
		// Less than
		case 7:
			if par1 < par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Equals
		case 8:
			if par1 == par2 {
				write(p, 1, i+3, (p[i]/10000)%10, relativeBase)
			} else {
				write(p, 0, i+3, (p[i]/10000)%10, relativeBase)
			}
			i += 4
		// Adjust relative base
		case 9:
			relativeBase += par1
			i += 2
		case 99:
			return output
		default:
			log.Fatal("Something went horably wrong")
		}
	}
}

func paramMode(program map[int]int, pointer int, mode int, relativeBase int) int {
	switch mode {
	// Position mode
	case 0:
		return program[program[pointer]]
	// Immediate mode
	case 1:
		return program[pointer]
	// Relative mode
	case 2:
		return program[relativeBase+program[pointer]]
	default:
		log.Fatal("Invalid parameter mode")
	}
	return -1
}

func write(program map[int]int, input int, pointer int, mode int, relativeBase int) {
	switch mode {
	// Position mode
	case 0:
		program[program[pointer]] = input
	// Immediate mode
	case 1:
		program[pointer] = input
	// Relative mode
	case 2:
		program[relativeBase+program[pointer]] = input
	default:
		log.Fatal("Invalid parameter mode")
	}
}

func readFile(path string, delimiter string) ([]int, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	s := strings.TrimSpace(string(f))

	numbers := []int{}
	for _, line := range strings.Split(s, delimiter) {
		i, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, i)
	}

	return numbers, nil
}
