package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCompleteIntcodeProgram(t *testing.T) {
	tests := []struct {
		input []int
		want  []int
	}{
		{
			input: []int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
			want:  []int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
		},
		{
			input: []int{1102, 34915192, 34915192, 7, 4, 7, 99, 0},
			want:  []int{1219070632396864},
		},
		{
			input: []int{104, 1125899906842624, 99},
			want:  []int{1125899906842624},
		},
	}

	for i, tt := range tests {
		name := fmt.Sprintf("Test %d", i)
		t.Run(name, func(t *testing.T) {
			programMap := make(map[int]int)
			for i, v := range tt.input {
				programMap[i] = v
			}
			got := executeProgram(programMap, 0)

			assert.Equal(t, tt.want, got)
		})
	}
}
