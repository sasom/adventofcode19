package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	reactions, err := readFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	ore := calculateOreForFuel(reactions, 1)
	fmt.Println(ore)

	fuel := guessFuelForOre(reactions, 1000000000000, ore)
	fmt.Println(fuel)
}

func guessFuelForOre(reactions map[string]reaction, availableOre int, oreFor1Fuel int) int {
	// Calculate estimate
	currentOre := 0
	fuel := availableOre / oreFor1Fuel
	step := fuel
	for {
		step /= 2
		currentOre = calculateOreForFuel(reactions, fuel)

		if availableOre > currentOre {
			fuel += step
		} else if availableOre < currentOre {
			fuel -= step
		}

		if step == 0 {
			break
		}
	}

	// Correct estimate
	for {
		if availableOre < currentOre {
			fuel--
			currentOre = calculateOreForFuel(reactions, fuel)
			if availableOre > currentOre {
				break
			}
		} else if availableOre > currentOre {
			fuel++
			currentOre = calculateOreForFuel(reactions, fuel)
			if availableOre < currentOre {
				fuel--
				break
			}
		}
	}

	return fuel
}

func calculateOreForFuel(reactions map[string]reaction, fuel int) int {
	need := make(map[string]int)
	need["FUEL"] = fuel
	for {
		count := 0
		for k, v := range need {
			// Skip because, we allready have those chemicals
			if v <= 0 {
				count++
				continue
			}

			multiply := 1
			// We need to create more than one chemical
			if reactions[k].output.amount != 0 &&
				need[k]-reactions[k].output.amount > 0 {
				multiply = need[k] / reactions[k].output.amount
			}
			// Create chemicals and remove them from needed
			need[k] -= reactions[k].output.amount * multiply

			// Add chemicals to needed
			for _, r := range reactions[k].input {
				need[r.name] += r.amount * multiply
			}
		}

		// Stop when only ore is positive
		if count == len(need)-1 && need["ORE"] > 0 {
			break
		}
	}
	return need["ORE"]
}

type chemical struct {
	name   string
	amount int
}

type reaction struct {
	input  []chemical
	output chemical
}

func readFile(path string) (map[string]reaction, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	s := strings.TrimSpace(string(f))

	reactions := make(map[string]reaction)
	rgx := regexp.MustCompile("(\\d+)\\s(\\w+)")
	for _, line := range strings.Split(s, "\n") {
		matches := rgx.FindAllStringSubmatch(line, -1)
		reaction := reaction{}

		for i := 0; i < len(matches); i++ {
			n, err := strconv.Atoi(matches[i][1])
			if err != nil {
				return nil, err
			}

			// Output
			if i == len(matches)-1 {
				reaction.output = chemical{
					name:   matches[i][2],
					amount: n,
				}
				continue
			}

			// Input
			reaction.input = append(reaction.input, chemical{
				name:   matches[i][2],
				amount: n,
			})
		}

		reactions[matches[len(matches)-1][2]] = reaction
	}

	return reactions, nil
}
