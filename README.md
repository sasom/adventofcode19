# AdventOfCode19

This repository contains solutions for [adventofcode](https://adventofcode.com) 2019 written in go. If example contains test cases, they will be included as test file.

Each day is a separate project. To run project type `make run` in project folder. For more commands type `make` or `make help`.